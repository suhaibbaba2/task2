<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include     $path."/Tasks/Task2/controller/home_controller.php";

/**
 * this function to show error message
 */
function Error_mail(){
    global $path;
    $error_type="Send Email";
    include($path."/Tasks/Task2/view/error_pages/error_page.php");
    die();
}

/**
 * this function to send email to database
 * @param $from
 * @param $to
 * @param $title
 * @param $body
 * @param $message
 * @param $active_link
 */
function myMail($from,$to,$title,$body,$message,$active_link)
{
    global $conn;
    $query=$conn->prepare("INSERT INTO mail (email_from,email_to,title,body,message,active_link) VALUES (?,?,?,?,?,?)");
    $query->bind_param("ssssss",$from,$to,$title,$body,$message,$active_link);
    if($query->execute())
    {
        header("Location: http://localhost/Tasks/Task2/");
    }else{
        Error_mail();
        die();
    }
    $query->close();
}

$code=$_GET['code'];
$user_email=$_GET['email'];

$message = "Your Activation Code is ".$code."";
$to=$user_email;
$title="Activation Code For Task 2";
$from = $user_email;
$body='Your Activation Code is '.$code.' Please Click On This link';
$active_link="http://localhost/Tasks/Task2/view/login_pages/verification.php?code=".$code;


myMail($from,$to,$title,$body,$message,$active_link);
echo "An Activation Code Is Sent To You Check You Emails";
