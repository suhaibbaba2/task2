<?php

$path = $_SERVER['DOCUMENT_ROOT'];
include $path . "/Tasks/Task2/view/header/header.php";
include $path . "/Tasks/Task2/controller/home_controller.php";


function Error_delete()
{
    global $path;
    $error_type = "Delete Data";
    include($path . "/Tasks/Task2/view/error_pages/error_page.php");
    die();
}

if (!isset($_GET['id']) || empty($_GET['id'])) {
    Error_delete();
    die();
}

$id = $_GET['id'];


if (auth() && auth_user(Get_userEmail($id))) {


    if (!($query = $conn->prepare("DELETE FROM blogs WHERE id =?"))) {
        Error_delete();
        die();
    }
    if (empty($id) || !$query->bind_param("i", $id)) {
        Error_delete();
        die();
    }

    if (!$query->execute()) {
        Error_delete();
        die();
    }
    $query->close();


    header("Location: http://localhost/Tasks/Task2/");
    die();

}
