<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include     $path."/Tasks/Task2/view/header/header.php";
include     $path."/Tasks/Task2/controller/home_controller.php";
if(auth_isActiveAccount()) {
    header("Location: http://localhost/Tasks/Task2");
}
$result=Get_Mail(auth_email());
?>
<style>
    .container
    {
        margin-top: 20px;
    }
    input,textarea{
        margin-bottom: 15px;
        width: 60% !important;
    }
    textarea{
        height: 150px !important;
    }
</style>
<title>Mail</title>
</head>
<body>
<div class="container">
    <label for="email-from">Email From</label>
    <input class="form-control" name="email-from" value="<?=$result['email_from']?>" disabled>
    <label for="email-from">Email To</label>
    <input class="form-control" name="email-to" value="<?=$result['email_to']?>" disabled>
    <label for="title">Title</label>
    <input class="form-control" type="title" value="<?=$result['title']?>" disabled>
    <label for="body">Message Body</label>
    <textarea class="form-control" name="body" disabled><?=$result['body']?></textarea>
    <label for="link">Activation Code</label>
    <a class="btn btn-primary" href="<?=$result['active_link']?>">Active Account</a>
</div>
</body>
</html
