<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include     $path."/Tasks/Task2/controller/home_controller.php";

/**
 * this function to show error message
 */

function Error_verification(){
    echo "error";
    global $path;
    $error_type="verification Email";
    include($path."/Tasks/Task2/view/error_pages/error_page.php");
    die();
}
if(!auth())
{
    Error_verification();
    die();
}
echo auth_email()."<br>";
$active_code=Get_activeCode(auth_email());
echo $active_code;
if(!isset($_GET['code']))
{
    Error_verification();
    die();
}

if($active_code==$_GET['code'])
{
    $is_active=1;
    if(!($edit_query=$conn->prepare("UPDATE users SET is_active = ? WHERE email = ?")))
    {
        Error_verification();
        die();
    }

    if(!$edit_query->bind_param("is",$is_active,auth_email()))
    {
        Error_verification();
        die();
    }
    if($edit_query->execute()){
        auth_SetisActiveAccount();
        $edit_query->close();

        header("Location: http://localhost/Tasks/Task2/");
    }
}