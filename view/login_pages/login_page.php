<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include     $path."/Tasks/Task2/view/header/header.php";
include     $path."/Tasks/Task2/controller/home_controller.php";
if(auth()){
    header("Location: http://localhost/Tasks/Task2/");
    die();
}
?>
<title>Login</title>
</head>
<body>
<div class="container">
    <div class="row text-center loginContainer">

        <?php
        if (isset($_SESSION["error_login"]) && $_SESSION['error_login'] == true) {
            echo "<div class='row center-block'>
                        <div class='alert alert-danger text-left center-block' style='width: 40%'><strong>Wrong!</strong> UserName or Password is Wrong</div>
                        </div> ";
            $_SESSION["error_login"]=false;
        }
        ?>

        <form id="loginForm" method="post" action="login_user_db.php">
            <h1>Login</h1>

            <div class="row text-center">
                <input type="email" class="form-control center-block text-center" name="email" placeholder="Email">
            </div>
            <div class="row text-center">
                <input type="password" class="form-control center-block text-center" name="password"
                       PLACEHOLDER="Password">
            </div>
            <div class="row">
                <button class="btn btn-primary" name="login">Login</button>
            </div>
            <a class="text-center" href="register_page.php"">Create New account</a>
        </form>

</body>
</html>