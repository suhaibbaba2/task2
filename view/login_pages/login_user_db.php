<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path."/Tasks/Task2/view/connect_database.php";


if(auth()){
    header("Location: http://localhost/Tasks/Task2/");
    die();
}
$email  =$_POST['email'];
$password=$_POST['password'];

$query = $conn->prepare("SELECT name,is_active From users where email = ? and password = ?");
$query->bind_param("ss", $email, sha1($password));
$query->bind_result($name,$is_active);
$query->execute();

if ($query->fetch() == 1)
{
    /**
     * to check if account is activated or not
     * and set Session ($is_active) 0 or 1
     */
    if($is_active==0)
    {
        $_SESSION['$is_active']=0;
        $_SESSION['error_login'] = false;
        $_SESSION['auth']=true;
        $_SESSION['user_email']=$email;
        $_SESSION['user_name']=$name;
        header("Location: http://localhost/Tasks/Task2/view/error_pages/error_activeCode.php");
        die();
    }
    $_SESSION['$is_active']=1;
    $_SESSION['error_login'] = false;
    $_SESSION['auth']=true;
    $_SESSION['user_email']=$email;
    $_SESSION['user_name']=$name;
    header("Location: http://localhost/Tasks/Task2/");
    die();
}
else {
    $_SESSION['error_login'] = true;
    header("Location: http://localhost/Tasks/Task2/view/login_pages/login_page.php");
    die();
}

