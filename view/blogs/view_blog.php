<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include     $path."/Tasks/Task2/view/header/header.php";
include     $path."/Tasks/Task2/controller/home_controller.php";

function Error_view(){
    global $path;
    $error_type="View Data";
    include($path."/Tasks/Task2/view/error_pages/error_page.php");
    die();
}

if(!isset($_GET['id'])||empty($id))
   // Error_view();

$id=$_GET['id'];

$result=Get_One_Blog($id);
?>
<title><?= $result['title'];?></title>
</head>
<body>

<?php
include $path."/Tasks/Task2/view/header/navbar.php";

?>

<div class="container create_blogs">
    <div class="page-header">
        <h1><?= $result['title'];?></h1>
    </div>

        <div class="row col-lg-8" style="margin-right:5px; ">
            <div class="">
                <p><?= $result['title'];?></p>
            </div>
            <div class="">
                <p><?= $result['body'];?></p>
            </div>
        </div>
        <div class="row col-lg-4">
            <?php if($result['images']!="none") { ?>
                <img src="../../public/upload_images/<?= $result['images']; ?>" class="img-responsive">
                <?php
            }
            ?>
        </div>

</div>

</body>
</html>