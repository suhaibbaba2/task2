<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include     $path."/Tasks/Task2/view/header/header.php";
include     $path."/Tasks/Task2/controller/home_controller.php";

if(!auth() || !auth_isActiveAccount())
{
    if(!auth_isActiveAccount())
    {
        header("Location: http://localhost/Tasks/Task2/view/error_pages/error_activeCode.php");
        die();
    }

    header("Location: http://localhost/Tasks/Task2/");
    die();
}
?>
<title>Create Blogs</title>
</head>
<body>

<?php
include $path."/Tasks/Task2/view/header/navbar.php";
?>
<div class="container create_blogs">
    <div class="page-header">
        <h1>Create Blogs</h1>
    </div>

    <form action="insert_blog_db.php" method="post" enctype="multipart/form-data">
        <div class="row " style="width: 60%">
            <div class="">
                <input type="text" class="form-control" name="title" placeholder="Title" required>
            </div>
            <div class="">
                <textarea type="text" class="form-control" name="body" placeholder="Body" required></textarea>
            </div>
            <div>
                <input type="file" class="" name="fileToUpload" id="fileToUpload">
            </div>
        </div>
        <input type="submit" class="btn btn-success " value="Create Blogs" name="submit" style="margin-left: 30%">

    </form>
</div>
</body>
</html>