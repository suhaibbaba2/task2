<?php

$path = $_SERVER['DOCUMENT_ROOT'];
include $path."/Tasks/Task2/view/connect_database.php";

$is_image=true;
/**
 * this function to show error message
 */
function Error_insert(){
    global $path;
    $error_type="Insert Data";
    include($path."/Tasks/Task2/view/error_pages/error_page.php");
    die();
}

/**
 * this function to check inputs is empty or not
 * @return bool
 */
function validate()
{
    global $is_image;
    if (!isset($_POST['title']) || empty($_POST['title']))
        return false;
    if(!isset($_POST['body']) || empty($_POST['body']))
        return false;
    if($_FILES['fileToUpload']['size']==0){
        $is_image=false;
    }
    return true;
}

if(!auth() || !auth_isActiveAccount()){
    if(!auth_isActiveAccount())
    {
        header("Location: http://localhost/Tasks/Task2/view/error_pages/error_activeCode.php");
        die();
    }
    Error_insert();
}
if(!validate())
{
    Error_insert();
    die();
}

$title=$_POST['title'];
$body=$_POST['body'];
if($is_image)
    $image_name=$_FILES['fileToUpload']['name'];
else
    $image_name="none";

if(!isset($_POST['submit'])||empty($_POST['submit']))
{
    Error_insert();
}

if($is_image)
    $image_dir=$path."/Tasks/Task2/public/upload_images/". basename($image_name);


if ($is_image)
    if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $image_dir));

else
{
    Error_insert();
}

if(!($insert_query=$conn->prepare("INSERT INTO blogs (title,body,images,user_email) VALUES (?,?,?,?)")))
{
    Error_insert();
    die();
}

if(!$insert_query->bind_param("ssss",$title,$body,$image_name,$_SESSION['user_email']))
{
    Error_insert();
    die();
}

if($insert_query->execute())
{
    $insert_query->close();
    header("Location: http://localhost/Tasks/Task2/");
}
else
{
    Error_insert();
}



