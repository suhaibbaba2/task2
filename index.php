<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include     $path."/Tasks/Task2/view/header/header.php";
include     $path."/Tasks/Task2/controller/home_controller.php";
?>
<title>Home</title>
</head>
<body>

<?php
include $path."/Tasks/Task2/view/header/navbar.php";
?>
<div class="container blogs">
    <?php
        $blogs=GetBlogs();
        if(count($blogs)==0)
            echo "<h1 class='text-center' style='font-weight: 600;font-size: 55px;color: brown'>No Data</h1>";
        for($i=0;$i<count($blogs);$i++){
        ?>
                <div class="row box">
                    <?php
                    if($blogs[$i]["images"]!="none") {
                        ?>
                        <img src="public/upload_images/<?= $blogs[$i]["images"]; ?>" style="width:100%">
                        <?php
                    }
                    ?>
                    <div class="center-block text-left">
                        <?php
                        if(auth() && auth_user(Get_userEmail($blogs[$i]["id"])) && auth_isActiveAccount())
                        {
                            ?>
                            <div class="edit_button text-right">
                                <a  href="view/blogs/edit_blogs.php?id=<?= $blogs[$i]["id"]?>" class="btn btn-warning">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </a>
                                <a href="Model/delete_blogs.php?id=<?= $blogs[$i]["id"]?>"
                                   onclick="return confirm('Are you sure you want to Delete Data?')"
                                   class="btn btn-danger">
                                    <i class="glyphicon glyphicon-remove"></i>
                                </a>
                            </div>

                            <?php
                        }
                        ?>
                        <a href="http://localhost/Tasks/Task2/view/blogs/view_blog.php?id=<?= $blogs[$i]['id'];?>">
                        <h3 class="text-left"><?= $blogs[$i]["title"];?></h3>
                        <p class="text-left">
                            <?= $blogs[$i]["body"];?>
                        </p>
                        </a>

                    </div>
                </div>
            <?php
    }
    ?>

</div>



</body>
</html>