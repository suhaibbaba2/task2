<?php

$path = $_SERVER['DOCUMENT_ROOT'];
include $path."/Tasks/Task2/view/connect_database.php";

if(auth()){
    header("Location: http://localhost/Tasks/Task2/");
    die();
}

/**
 * this function to check inputs is empty or not
 * @return bool
 */
function validate()
{
    if (!isset($_POST['name']) || empty($_POST['name']))
    {
        echo "name";
        return false;
    }
    if(!isset($_POST['email'])&& empty($_POST['email']))
    {
        echo "email";
        return false;
    }
    if(!isset($_POST['password'])&& empty($_POST['password']))
    {
        echo "password";
        return false;
    }

    return true;
}

/**
 * this function to show error message
 */
function Error_Register ()
{
    global $path;
    $error_type="Register";
    include($path."/Tasks/Task2/view/error_pages/error_page.php");
    die();
}

if(!validate())
{
    echo "validate";
    Error_Register();
    die();
}

$username=$_POST['name'];
$email  =$_POST['email'];
$password=$_POST['password'];

$_SESSION['user_email']=$email;

$code=rand(1000,9999);

$query = $conn->prepare("INSERT INTO users (name,password,email,active_code) VALUES (?,?,?,?)");

$query->bind_param("sssi",$username,sha1($password),$email,$code);

if($query->execute())
{
    $_SESSION['error_register']=false;
    header("Location: http://localhost/Tasks/Task2/view/login_pages/activate_email.php?code=".$code."&email=".$email);
    die();
}
else
{
    echo "dd";
    Error_Register();
}