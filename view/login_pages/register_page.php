<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include     $path."/Tasks/Task2/view/header/header.php";
include     $path."/Tasks/Task2/controller/home_controller.php";
if(auth()){
    header("Location: http://localhost/Tasks/Task2/");
   die();
}
?>
<title>Register</title>
</head>
<body>
<div class="container">
    <div class="row text-center loginContainer">

        <?php
        if (isset($_SESSION["error_register"]) && $_SESSION['error_register'] == true)
        {
            $_SESSION['error_register']=false;
            echo "<div id='error_register' class='row center-block' style='display: none'>
                        <div class='alert alert-danger text-left center-block' style='width: 40%'><strong>Wrong!</strong> ERROR In Register Try Agian!!</div>
                        </div> ";
        }
        ?>
        <form id="registerForm" method="post" action="register_user_db.php">
            <h1>Register</h1>
            <div class="row text-center">
                <input type="text" class="form-control center-block text-center" name="name"
                       placeholder="User Name">
            </div>
            <div class="row text-center">
                <input type="email" class="form-control center-block text-center" name="email" placeholder="Email">
            </div>

            <div class="row text-center">
                <input type="password" class="form-control center-block text-center" name="password"
                       PLACEHOLDER="Password">
            </div>
            <div class="row text-center">
                <input type="password" class="form-control center-block text-center" name="confirmPassword"
                       PLACEHOLDER="Confirm Password">
            </div>


            <div class="row">
                <button class="btn btn-primary" name="register">Register</button>
            </div>
            <a class="text-center" href="login_page.php"">Login</a>

        </form>


    </div>
</div>
</body>
</html>