<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include     $path."/Tasks/Task2/view/header/header.php";
include     $path."/Tasks/Task2/controller/home_controller.php";

/**
 * this function to show error message
 */
function Error_edit(){
    global $path;
    $error_type="Edit Data";
    include($path."/Tasks/Task2/view/error_pages/error_page.php");
    die();
}

if(!isset($_GET['id'])||empty($_GET['id'])) {
    Error_edit();
}

$id=$_GET['id'];

if(!auth() || !auth_user(Get_userEmail($id)) || !auth_isActiveAccount())
{
    if(!auth_isActiveAccount())
    {
        header("Location: http://localhost/Tasks/Task2/view/error_pages/error_activeCode.php");
        die();
    }
    header("Location: http://localhost/Tasks/Task2/");
    die();
}
?>
<title>Edit Blogs</title>
</head>
<body>

<?php
include $path."/Tasks/Task2/view/header/navbar.php";

$result=Get_One_Blog($id);

?>

<div class="container create_blogs">
    <div class="page-header">
        <h1>Edit Blogs</h1>
    </div>

    <form action="update_blog_db.php" method="post" enctype="multipart/form-data">
        <div class="row col-lg-8" style="margin-right:5px; ">
            <div class="">
                <input type="text" class="form-control" name="id" value="<?= $id;?>" style="display: none">
            </div>
            <div class="">
                <input type="text" class="form-control" name="title" placeholder="Title" value="<?= $result['title'];?>" required>
            </div>
            <div class="">
                <textarea type="text" class="form-control" name="body" placeholder="Body" required><?= $result['body'];?></textarea>
            </div>
            <div>
                <label class="btn btn-primary" for="fileToUpload" style="display: none">sss</label>
                <input type="file" class="btn btn-primary" name="fileToUpload" id="fileToUpload">
            </div>
            <div style="display: none">
                <input type="text" class="btn btn-primary" name="old_image" value="<?= $result['images'];?>">
            </div>
        </div>
        <div class="row col-lg-4">
            <?php
            if($result['images']!="none") {
                ?>
                <img src="http://localhost/Tasks/Task2/public/upload_images/<?= $result['images']; ?>"
                     class="img-responsive">
                <?php
            }
            ?>
        </div>
        <input type="submit" class="btn btn-success " value="Edit Blogs" name="submit" style="margin-left: 30%">

    </form>
</div>

</body>
</html>