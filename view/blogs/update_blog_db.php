<?php

$path = $_SERVER['DOCUMENT_ROOT'];
include $path."/Tasks/Task2/view/connect_database.php";

$newImage=true;

/**
 * this function to show error message
 */
function Error_edit(){
    global $path;
    $error_type="Update Data";
    include($path."/Tasks/Task2/view/error_pages/error_page.php");
    die();
}

/**
 * this function to check inputs is empty or not
 * @return bool
 */
function validate()
{
    global $newImage;

    if (!isset($_POST['title']) || empty($_POST['title']))
        return false;
    if(!isset($_POST['body']) || empty($_POST['body']))
        return false;
    if($_FILES['fileToUpload']['size']==0)
        $newImage=false;
    return true;
}

if(!isset($_POST['id'])||empty($_POST['id'])) {
    Error_edit();
}

$id = $_POST['id'];

if(!auth() || !auth_user(Get_userEmail($id)) || !auth_isActiveAccount()){
    if(!auth_isActiveAccount())
    {
        header("Location: http://localhost/Tasks/Task2/view/error_pages/error_activeCode.php");
        die();
    }
    Error_edit();
}

if(!validate())
{
    Error_edit();
    die();
}

$title=$_POST['title'];
$body=$_POST['body'];

/**
 * it is a bool
 * and it is set in validate function
 */
if($newImage) {
    $image_name = $_FILES['fileToUpload']['name'];
}
else {
    $image_name = $_POST['old_image'];
}

if(!isset($_POST['submit'])||empty($_POST['submit']))
{
    Error_edit();
}
if($newImage) {
    $image_dir = $path . "/Tasks/Task2/public/upload_images/" . basename($image_name);

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $image_dir)) ;
    else {
        Error_edit();
    }
}
if(!($edit_query=$conn->prepare("UPDATE blogs SET title = ?,body = ?,images = ? WHERE id = ?")))
{
    Error_edit();
    die();
}
if(!$edit_query->bind_param("sssi",$title,$body,$image_name,$id))
{
    Error_edit();
    die();
}
if($edit_query->execute()){
    $edit_query->close();

    header("Location: http://localhost/Tasks/Task2/");
}
else{
    Error_edit();
}



