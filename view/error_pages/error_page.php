<?php
$path = $_SERVER['DOCUMENT_ROOT'];
//include     $path."/Tasks/Task2/controller/home_controller.php";
include $path . "/Tasks/Task2/view/header/header.php";

function error_ErrorPage()
{
    //header("Location: http://localhost/Tasks/Task2/");
    die();
}

if (!auth()) {
    error_ErrorPage();
}

?>

<title>ERROR Page</title>
</head>
<body>

<?php
include $path . "/Tasks/Task2/view/header/navbar.php";
?>

<div class="container error_page text-center">
    <div class="row center-block text-center">
        <i id ="remove_icon" class="glyphicon glyphicon-remove-circle"></i>
    </div>
    <div class="row ">
        <h1>ERROR 404</h1>
    </div>
    <div class="row">
        <h3>ERROR in <?= $error_type;?></h3>
    </div>
</div>

</body>
</html>
