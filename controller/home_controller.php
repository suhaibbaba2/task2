<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path."/Tasks/Task2/view/connect_database.php";


/**
 * this function to show error message
 */
function Error_selectData(){
    global $path;
    $error_type="Get Data";
    include($path."/Tasks/Task2/view/error_pages/error_page.php");
    die();
}

/**
 * this function return all blogs from database
 * @return array
 */
function GetBlogs()
{
    global $conn;
    if(!($query = $conn->prepare("SELECT id,title,body,images,user_email From blogs order by id DESC ")))
    {
        Error_selectData();
        die();
    }
    if(!($query->bind_result($id, $title, $body, $images, $user_email)))
    {
        Error_selectData();
        die();
    }
    if(!$query->execute())
    {
        Error_selectData();
        die();
    }

    $result = array();
    while ($query->fetch()) {
        $myarray = array("id" => $id, "title" => $title, "body" => $body, "images" => $images, "user_email" => $user_email);
        array_push($result, $myarray);

    }
    $query->close();
    return $result;

}

/**
 * this function return one blog
 * @param $id
 * @return array
 */
function Get_One_Blog($id)
{
    global $conn;
    if(!($query = $conn->prepare("SELECT id,title,body,images,user_email From blogs WHERE id = ?")))
    {
        Error_selectData();
        die();
    }
    if(!$query->bind_param("i",$id))
    {
        Error_selectData();
        die();
    }
    if(!$query->bind_result($id, $title, $body, $images, $user_email)){
        Error_selectData();
        die();
    }
    if(!$query->execute())
    {
        Error_selectData();
        die();
    }
    if(!$query->fetch()){
        Error_selectData();
        die();
    }
    $result = array("id" => $id, "title" => $title, "body" => $body, "images" => $images, "user_email" => $user_email);
    $query->close();

    return $result;

}



