<?php

/**
 * this function returns "user email" that created blog
 * @param $id is a id for blogs table (primary key) to specific user email for this blog
 * * @return string contain user email
 */
function Get_userEmail($id)
{
    global $conn;
    $select_email = $conn->prepare("SELECT user_email FROM blogs WHERE id=?");
    $select_email->bind_param("i", $id);
    $select_email->bind_result($user_email);
    $select_email->execute();
    $select_email->fetch();
    $select_email->close();

    return $user_email;
}

/**
 * this function returns username that logged in
 * @return username
 */
function Get_userName()
{
    return $_SESSION['user_name'];
}

/**
 * this function returns bool value(true or false) if the user logged in or not
 * @return bool
 */
function auth(){
    if(isset($_SESSION['auth']) && $_SESSION['auth']=true){
        return true;
    }
    else{
        return false;
    }
}

/**
 * this function returns bool to check if user account is activated or not
 * @return bool
 */
function auth_isActiveAccount()
{
    if( isset($_SESSION['$is_active'])&&$_SESSION['$is_active']==1)
        return true;
    else
        return false;
}

/**
 * this function to set Session("is_active) = 1
 */
function auth_SetisActiveAccount()
{
    $_SESSION['$is_active']=1;
}

/**
 * this function to check if user email that own blog is the same user who logged in
 * @param $user_email
 * @return bool
 */
function auth_user($user_email)
{
    if($user_email==$_SESSION['user_email'])
        return true;
    else
        return false;
}

/**
 * this function return active code for user from database
 * @param $email
 * @return mixed
 */
function Get_activeCode($email)
{
    global $conn;
    echo $email."<br>";
    $getActiveCode = $conn->prepare("SELECT active_code FROM users WHERE email=?");
    $getActiveCode->bind_param("s", $email);
    $getActiveCode->bind_result($active_code);
    $getActiveCode->execute();
    $getActiveCode->fetch();
    $getActiveCode->close();
    echo $active_code;
    return $active_code;
}

/**
 * this function returns user email that logged in
 * @return mixed
 */
function auth_email(){
    return $_SESSION['user_email'];
}

/**
 * this function to get email information from database
 * @param $email
 * @return array
 */
function Get_Mail($email)
{
    global $conn;
    $select_email = $conn->prepare("SELECT * FROM mail WHERE email_from=?");
    $select_email->bind_param("s", $email);
    $select_email->bind_result($id,$email_from,$email_to,$title,$body,$message,$active_link);
    $select_email->execute();
    $select_email->fetch();

    $select_email->close();
    $result=array("email_from"=>$email_from,"email_to"=>$email_to,"title"=>$title,
                    "body"=>$body,"message"=>$message,"active_link"=>$active_link);
    return $result;
}
