<?php
$path = $_SERVER['DOCUMENT_ROOT'];
?>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost/Tasks/Task2/index.php">Task 2</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="http://localhost/Tasks/Task2/index.php">Home</a></li>
            <li id="rss_icon"><a href="http://localhost/Tasks/Task2/view/rss_feed.php"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
        </ul>


        <ul class="nav navbar-nav navbar-right">

        <?php
        if(!auth()) {
            ?>
                <li><a href="http://localhost/Tasks/Task2/view/login_pages/register_page.php"><span class="glyphicon glyphicon-user"></span> Sign
                        Up</a></li>
                <li><a href="http://localhost/Tasks/Task2/view/login_pages/login_page.php"><span class="glyphicon glyphicon-log-in"></span> Login</a>
                </li>
            <?php
        }else {
            ?>
            <li><a href="http://localhost/Tasks/Task2/view/blogs/create_blogs.php"><i class="glyphicon glyphicon-plus"></i>&nbsp; Create Blogs</a></li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-user"></i> <?=Get_userName();?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="http://localhost/Tasks/Task2/view/mail.php">Mail</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="http://localhost/Tasks/Task2/view/login_pages/logout_page.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                </ul>
            </li>


            <?php
        }

        ?>
        </ul>
    </div>
</nav>
