<?php echo '<?xml version="1.0" encoding="UTF-8" ?>' ?>

<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include $path . "/Tasks/Task2/controller/home_controller.php";
$result = GetBlogs();
?>
<rss version="2.0">
    <channel>
        <title>Taks 2</title>
        <link>http://localhost/Tasks/Task2/</link>
        <description>Task 2</description>

        <?php
        for($i=0;$i<count($result);$i++) {
            ?>
            <item>
                <title><?= $result[$i]['title'];?></title>
                <link>
                http://localhost/Tasks/Task2/view/blogs/view_blog.php?id=<?= $result[$i]['id'];?></link>
                <description><?= $result[$i]['body'];?></description>
            </item>
            <?php
        }
        ?>
    </channel>
</rss>
